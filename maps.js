function initMap() { 

	options = {
		center: {lat: 37.74977073928103, lng: -122.39242219446099}, 
		zoom: 14,
	}

	map = new google.maps.Map(document.getElementById('map'), options);

	var markersArray = [];
	arraySpeed = [];
	arrayLat = [];
	arrayLong = [];
	var color = "#000000";

	function addMarker(lat, lng, color) {
		marker = new google.maps.Marker(
			{
				position: {lat: lat,lng: lng},
				map:map,
				icon: color,
			}
		)
		markersArray.push(marker); //Stores locations of markers to be deleted later
	}

	function massAddMarker(){
		for (i = 0; i < arrayLat.length; i++){
			setIcon();
			addMarker(arrayLat[i], arrayLong[i], arrayColor[i]);
		}
		map.panTo({lat:arrayLat[0], lng:arrayLong[0]});
	}	

	function deleteMarkers(){
		arrayLat = [];
		arrayLong = [];
		for (var i = 0; i < markersArray.length; i++) {
			markersArray[i].setMap(null);
		}
	}

	function readJSONFiles(urllink) {
		$.ajax({
			url: urllink,
			dataType: "JSON",
			type: "GET",
			success: function(data) {
				$(data.coords).each(function(index, value){
					arrayLat.push(value.lat)
					arrayLong.push(value.lng);
					arraySpeed.push(value.speed);
				});
				massAddMarker();
			}
		}) 
	}

	//When the website first loads it will run this
	readJSONFiles("jsonFiles/2016-07-02--11-56-24.json");

	$("#dateList").change(function(){
		deleteMarkers();
		formChanged();
		readJSONFiles(dateTime);
	})
}