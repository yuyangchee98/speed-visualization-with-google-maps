from ovalGenerator import *

fileName = "setIcon.js"
f = open(fileName, 'w')

f.write("""
var arrayColor = [];
function setIcon(){
	""")

x = -1
y = 0

for element in colorList:

	element = str(element)
	element = element.replace("<Color ", "")
	element = element.replace(">", "")

	if (x==-1 and y ==0):
		f.write(
	''' 
	if( arraySpeed[i] == 0){
			arrayColor[i] = ''' + "'./ovals/" + element.replace("#", "") + ".svg'" + '''
		}
	''')
	elif (x > -1 and y > 0):
		f.write(
	''' 
	else if( ''' + str(x) + '''< arraySpeed[i] && arraySpeed[i] <= ''' + str(y) + '''){
			arrayColor[i] = ''' + "'./ovals/" + element.replace("#", "") + ".svg'" + '''
		}
	''')		
	
	x=x+0.5
	y=y+0.5


f.write("""
	}
""")

f.close()