function initMap() {

	options = {
		center: {lat: 37.74977073928103, lng: -122.39242219446099}, 
		zoom: 13,
	}

	map = new google.maps.Map(document.getElementById('map'), options);

	/* var markersArray = [];

	function addMarker(lat,lng) {
		marker = new google.maps.Marker(
			{
				position: {lat: lat,lng: lng},
				map:map,
				iconscale: 0.04,	
			}
		)
		markersArray.push(marker); //Stores locations of markers to be deleted later
	}

	function massAddMarker(){
		for (i = 0; i < arrayLat.length; i++){
			addMarker(arrayLat[i], arrayLong[i]);		
		}
	}
	*/

	

	arrayLat = [];
	arrayLong = [];
	var heatMapDatapoints = [];
	var maxSpeed = -1;
	var minSpeed = 99;

	function readJSONFiles(urllink) {
		$.ajax({
			url: urllink,
			dataType: "JSON",
			type: "GET",
			success: function(data) {
								
				$(data.coords).each(function(index, value){
					if (value.speed > maxSpeed){
						maxSpeed = value.speed;
					}
					
					if (value.speed < minSpeed){
						minSpeed = value.speed;
					}
				});

				console.log("max =" + maxSpeed)
				console.log("min =" + minSpeed)

				$(data.coords).each(function(index, value){

					heatMapDatapoints.push(
						{location: new google.maps.LatLng(value.lat, value.lng),
						weight: value.speed / maxSpeed}
					);
				});
				setOverlay(map)
				setGradient();
			}
		}) 
	}

	function setGradient() {
		gradient = [
			'rgba(0, 255, 255, 0)',			
			'#148CFF',
			'#138DFA',
			'#138EF6',
			'#128FF1',
			'#1290ED',
			'#1191E9',
			'#1192E4',
			'#1193E0',
			'#1094DB',
			'#1095D7',
			'#0F96D3',
			'#0F97CE',
			'#0F98CA',
			'#0E99C5',
			'#0E9AC1',
			'#0D9BBD',
			'#0D9CB8',
			'#0D9DB4',
			'#0C9EB0',
			'#0C9FAB',
			'#0BA0A7',
			'#0BA1A2',
			'#0BA29E',
			'#0AA39A',
			'#0AA495',
			'#09A591',
			'#09A68C',
			'#08A788',
			'#08A884',
			'#08A97F',
			'#07AA7B',
			'#07AB76',
			'#06AC72',
			'#06AD6E',
			'#06AE69',
			'#05AF65',
			'#05B061',
			'#04B15C',
			'#04B258',
			'#04B353',
			'#03B44F',
			'#03B54B',
			'#02B646',
			'#02B742',
			'#02B83D',
			'#01B939',
			'#01BA35',
			'#00BB30',
			'#00BC2C',
			'#00BD28',
		];
		heatmap.set('gradient', gradient);
	}

	//Deleting markers
	function deleteOverlays(){
		arrayLength = heatMapDatapoints.length
		for (i = 0; i < arrayLength; i++){
			heatMapDatapoints.pop(i);
		}
		heatmap.setMap(null);
	}
	//END

	var heatmap = new google.maps.visualization.HeatmapLayer({
		data: heatMapDatapoints,
		radius: 3,
		maxIntensity: 20,
	});

	function setOverlay(map){
		heatmap.setMap(map);
	}

	setOverlay(map);

	//When the website first loads it will run this
	readJSONFiles("jsonFiles/2016-07-02--11-56-24.json");

	$("#dateList").change(function(){
		deleteOverlays();
		formChanged();
		console.log(dateTime);
		readJSONFiles(dateTime);
	})
}