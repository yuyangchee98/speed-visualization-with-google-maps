from colour import *

first = Color("#f02b00")
colorList = list(first.range_to("#63ff14", 100))

print(colorList)

for element in colorList:

	element = str(element)
	element = element.replace("<Color ", "")
	element = element.replace(">", "")

	fileName = "./ovals/" + element.replace("#", "") + ".svg"
	f = open(fileName, 'w')
	
	f.write('''<?xml version="1.0" encoding="UTF-8"?>
		<svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<title>Oval</title>
				<g transform="translate(-2.000000, -2.000000)" fill=" ''' + element + ' " stroke=" ' + element + ''' ">
					<circle id="Oval" cx="9" cy="9" r="5"></circle>
				</g>
		</svg>''')

	f.close()