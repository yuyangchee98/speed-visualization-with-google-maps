<!DOCTYPE html>
<html>

<head>
  <title>Simple Map</title>
  <meta name="viewport" content="initial-scale=1.0">
  <meta charset="utf-8">
  <link rel="stylesheet" href="main.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="./setIcon.js"></script>
  <script src="./jsonFiles/2016-07-02--11-56-24.json"></script>

  <?php
    $jsonFileNames = glob('jsonFiles/*.json');
  ?>
  <script>
    var jsonFileNames = <?php echo json_encode($jsonFileNames); ?>;

    var dateTime = "jsonFiles/2016-07-02--11-56-24.json";
    
    function formChanged() {
      dateTime = document.getElementById("dateList").value;
      dateTime = "jsonFiles/" + dateTime + ".json";
      dateTime = dateTime.replace("  ", "--");
    }
  </script>

</head>

<body>

  <div class = "floating select-style">
      <select name="dateList" id="dateList" onchange="formChanged()">
        <?php
          foreach($jsonFileNames as $date) {
            $datePresentation =  str_replace("jsonFiles/", "", $date);
            $datePresentation =  str_replace(".json", "", $datePresentation);
            $datePresentation =  str_replace("--", "  ", $datePresentation);
            echo '<option value="' . $datePresentation . '">' . $datePresentation . '</option>';
          }
        ?>
      </select>
  </div>

  <div class = "floating legend">
    <div class = "text">
    </div>
    <div id = "gradientGrid">
    </div>
    <div class = "text value">
      <a class="left">0mph</a>
      <a class="right">50mph</a>
    </div>
  </div>

  <div id="map"></div>
  <script src="maps.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABJ-2ksqb6FyBBSYLP8JTc2xQLoPa5O8w&callback=initMap&libraries=visualization" async defer></script>
  
</script>
</body>

</html>